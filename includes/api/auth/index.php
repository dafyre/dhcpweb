<?PHP
require '../../../config.php';

if (isset($_POST['txtDebug'])) {$debug=true;} else {$debug=false;}


if (isset($_POST['txtUser']) && isset($_POST['txtPass'])) {
    $txtUser = $_POST['txtUser'];
    $txtPass = $_POST['txtPass'];
    if ($debug) {echo "Passed $txtUser, $txtPass <br/>";}

    $user=R::findOne('users',' user_name=:userName ', [':userName'=>$txtUser]);

    if (!$user) {
        $response=array('status'=>'-1','token'=>'Invalid Uesr');
    }

    if ($user) {
        $verified=password_verify ($txtPass,$user->password);
        if ($debug==true) {echo "Verified says $verified";}
        if (!$verified==true) {
            array('status'=>'-10','token'=>'Invalid Login Information');
        }

        if ($verified == true) {
            $passHash=$user->password;
            $newToken=password_hash("$txtUser$passHash",PASSWORD_DEFAULT);
            $user->token=$newToken;
            $user->tokenExpires=strtotime('+10 minutes');
            R::store($user);
            $response=array('status'=>'verified','token'=>$newToken);
        }
    }

    if ($debug == true) {echo print_r($response);}

    echo json_encode($response);    
} else {
    $response=array('status'=>'invalid','token'=>'Invalid Data');
    echo json_encode($response);
}
/*
AUTHENTICATION API
*/

/*
  public function auth($args) {
    $debug=false;
    $authInfo=json_decode($args);
    $userName=$authInfo->postData->txtUsername;
    $password=$authInfo->postData->txtPassword;
    $response="";

    $user=R::findOne('users',' user_name=:userName ' ,[':userName'=>$userName]);
    if (!$user) {
      $response=array('status'=>'-1', 'token'=>'Invalid User');
    }

    if ($user) {
      if ($debug) echo "Verifying user...";
      $verified=password_verify($password,$user->password);
      if ($debug) echo "Is password verified? $verified";
      if ($debug) print_r($user);
      if ($verified==true) {
        $passHash=$user->password;
        $newToken=password_hash("$userName$passHash",PASSWORD_DEFAULT);
        $user->token=$newToken;
        $user->tokenExpire=strtotime('+10 minutes');
        R::store($user);
        $response=array('status'=>'verified','token'=>$newToken);
      }
    }
    return json_encode($response);
  }
*/
