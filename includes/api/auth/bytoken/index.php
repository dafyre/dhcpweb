<?PHP

require '../../../../config.php';

if (isset($_POST['txtDebug'])) {$debug=true;} else {$debug=false;}

if (!isset($_POST['token'])) {
    $response=array('status'=>'invalid','token'=>'Invalid Data');
    echo json_encode($response);
    return false;
}

$token=$_POST['token'];

$user=R::findOne('users',' token=:userToken ', [':userToken'=>$token]);

if (!$user) {
    $response=array('status'=>'invalid_token',token=>'Invalid Token!');
    echo json_encode($response);
    return false;
}

$curTime=time();
$tokenExpire=$user->tokenExpires;

if ($curTime > $tokenExpire) {
    //token has exxpired!
    $response=array('status'=>'expired','token'=>'Expired Token!');
    echo json_encode($reponse);
    return false;
} else {
    //token has NOT expired, let's update the time.
    $newExpiry=strtotime('+10 minutes');
    $user->tokenExpires=$newExpiry;
    R::store($user);
    $response=array('status'=>'verified','token'=>$token);
    echo json_encode($response);
    return false;
}