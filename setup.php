<?php
require 'config.php';
if (!isset($_SESSION['setupStep'])) {
    echo "Session not defined!";
    $_SESSION['setupStep']=0;
}

if ($_SESSION['setupStep']==0) {
$userName='admin';
$password='admin';
$password=password_hash($password,PASSWORD_DEFAULT);

$authUser=R::Dispense('users');
$authUser->firstName='System';
$authUser->lastName='Administrator';
$authUser->password=$password;
$authUser->userName=$userName;
$authUser->token='';
$authUser->tokenExpires=0;
$authUser->forcePassChange=1;

R::store($authUser);

echo "Admin user created. <br/>";
$_SESSION['setupStep']=1;

}

if ($_SESSION['setupStep']==1) {
    echo "Done!";
}



echo "Session says ".$_SESSION['setupStep'];
