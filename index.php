<?php
require 'config.php';
?>
<link href='theme/default.css' rel='stylesheet'></link>

<script src='js/jquery-3.3.1.min.js' type='text/javascript' laguage='javascript'></script>
<script src='js/functions.js' type='text/javascript' laguage='javascript'></script>

<div id='divSiteHeader'>DHCPWeb</div>
<div id='divLoginWindow'>

</div>


<script type='text/javascript' language='javascript'>
 var token=getCookie('token');
 if (token.length>0) {
     console.log('Authenticating by Token...');
     $.post('includes/api/auth/bytoken/',{token:token},function(data){
         data=JSON.parse(data);
         if (data.status=='verified') {
            $("body").load("main.html");
         }
     })

 }
 $('#divLoginWindow').load('includes/loginWindow.html');
</script>
